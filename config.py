"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os


class RRIGConfig:
    # devs home
    HOME = os.path.expanduser('~')

    # try '.' directory first
    if os.path.exists('data/tile_imgs'):
        DIR_PROJECT = "."
    else:
        DIR_PROJECT = os.path.join(HOME, 'Work', 'Projects', 'RummikubSolver', 'Code')

    DIR_DATA = os.path.join(HOME, 'Work', 'Projects', 'RummikubSolver', 'Data')
    # DIR_IMGS = os.path.join(DIR_DATA, 'Images')
    DIR_REF_IMGS = os.path.join(DIR_PROJECT, 'data', 'tile_imgs')
