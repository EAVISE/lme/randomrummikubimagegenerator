"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import json
import math
import os.path
import shutil
from collections import Counter, OrderedDict
from typing import Tuple

import numpy as np
from PIL import Image
from numpy.random import default_rng

from config import RRIGConfig
from rrig.ref_img_loader import RefImgLoader, RefImgs


class NPEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NPEncoder, self).default(obj)


class ImageGenerator:
    def __init__(self,
                 b_add_pos_noise=False,
                 max_pos_offset=20,
                 b_add_rot_noise=False,
                 max_rot_angle=2):
        """

        :param b_add_pos_noise: add positional noise --> slight random x/y offsets between consecutive tiles
        :param max_pos_offset: maximum offset in pixels when applying positional noise
        :param b_add_rot_noise: add rotational noise --> individual tiles will be slightly randomly rotated
        :param max_rot_angle: maximum rotation angle in degrees (!) when applying rotational noise
        """
        self.ril = RefImgLoader()
        self.ref_names = RefImgs()
        self.rng = default_rng()

        self.width, self.height = None, None

        self.b_add_pos_noise = b_add_pos_noise
        self.b_add_rot_noise = b_add_rot_noise
        self.max_pos_offset, self.max_rot_angle = max_pos_offset, max_rot_angle

        self.resize = 0.4  # by what ratio to resize the tiles, '1' for no resizing

    def generate_valid_img(self, width=None, height=None,
                           max_nb_series=4,
                           max_series_len=13,
                           b_use_jokers=True,
                           b_disco_mode=True,
                           bg_color=(255, 255, 255, 255),
                           b_show=False):
        """
        This method will generate a random image containing valid Rummikub tile groups.

        :param width: image width in pixels, if 'None' will be set automatically
        :param height: image heigth in pixels, if 'None' will be set automatically
        :param max_nb_series: maximum number of series generate
        :param max_series_len: maximum length of any one series (max. is 13, so no series longer than this will be
         generated); should be 3 <= max_series_len <=13
        :param b_use_jokers: allow the usage of jokers or not, 'True' by default
        :param b_disco_mode: randomly generate a background color!
        :param bg_color: background color to use if b_disco_mode is False
        :param b_show: show generated image
        :return:
        """
        if max_series_len < 3 or max_series_len > 13:
            raise ValueError(f"Invalid max_series_len, should be 3 <= x <= 13, got {max_series_len}.")

        # Keep track of used tiles
        tile_counter = Counter()
        if not b_use_jokers:
            for e in self.ref_names.jokers:
                tile_counter[e] = 1

        # Set width such that it can surely contain the maximum length series
        if width is None:
            width = int(100 + (max_series_len * self.ril.img_width * self.resize))
            if self.b_add_pos_noise:
                width += (max_series_len-1) * self.max_pos_offset
        self.width = width
        # Set height such that it can surely contain the maximum number of series
        if height is None:
            height = int(150 + (max_nb_series * self.ril.img_heigth * self.resize))
            if self.b_add_pos_noise:
                height += (max_nb_series-1) * self.max_pos_offset
        self.height = height

        bboxes = OrderedDict()
        series_bboxes = OrderedDict()

        if b_disco_mode:
            bg_color = tuple(self.rng.integers(0, 255, 3, endpoint=True)) + (255,)
        img = Image.new(mode='RGBA', size=(width, height), color=bg_color)

        starting_point = (self.rng.integers(25, 75, endpoint=True), self.rng.integers(50, 100, endpoint=True))
        curr_pos = starting_point

        nb_series = self.rng.integers(1, max_nb_series, endpoint=True)
        for _ in range(nb_series):
            # First, decide what we are gonna do: a '1-2-3' series, or a 'color' series?
            # Let's say: <0.5 = '1-2-3', >=0.5 = 'color'.

            # bbox of the entire series: UL, UR, BL, BR
            series_bbox = [[math.inf, math.inf], [-math.inf, math.inf],
                           [math.inf, -math.inf], [-math.inf, -math.inf]]
            tile_bboxes = []

            # Generate '1-2-3' series.
            if self.rng.random() < 0.5:
                # rotate_angle = random.randint(-5, 5)  # Not needed for now
                # Get valid random series
                b_valid = False
                nb_iters = 0  # Let's build in an infinite-loop breaker, just in case...
                while b_valid is False:
                    nb_iters += 1
                    if nb_iters == 100:
                        raise StopIteration("This thing has been going on for a while now. It would appear something "
                                            "might be off?")
                    # Get starting tile
                    series_len = self.rng.integers(3, max_series_len, endpoint=True)
                    tile_name, tile_color, tile_number = self.ref_names.get_random_tile(max_number=14-series_len)
                    b_valid = self._check_num_series(tile_counter, tile_color, tile_number, tile_number+series_len)

                # Generate series
                for i in range(0, series_len):
                    tile_name = self.ref_names.get_tile(color=tile_color, number=(tile_number+i) % 13)
                    # Randomly replace tile with joker in some cases
                    tile_name = self._random_joker(tile_counter=tile_counter, tile_name=tile_name, prob=0.05)
                    tile_counter[tile_name] += 1

                    # Add tile to image
                    curr_pos, img, bbox = self._add_tile(img, tile_name, input_pos=curr_pos,
                                                         b_update_y=(i == series_len-1))
                    # Update bboxes
                    tile_bboxes.append((tile_name, bbox))
                    self._update_series_bbox(series_bbox, bbox)

                new_pos_x = self.rng.integers(25, 75, endpoint=True)
                new_pos_y = curr_pos[1] + self.rng.integers(50, 100, endpoint=True)
                curr_pos = (new_pos_x, new_pos_y)
                bboxes[f'123_{tile_color}_{tile_number}_{tile_number+series_len-1}'] = tile_bboxes
                series_bbox = tuple(tuple(x) for x in series_bbox)
                series_bboxes[f'123_{tile_color}_{tile_number}_{tile_number+series_len-1}'] = series_bbox
            # Generate 'color' series
            else:
                b_valid = False
                nb_iters = 0  # Let's build in an infinite-loop breaker, just in case...
                while b_valid is False:
                    nb_iters += 1
                    if nb_iters == 100:
                        raise StopIteration("This thing has been going on for a while now. It would appear something "
                                            "might be off?")
                    # Get random number
                    rand_nb = self.rng.integers(1, 13, endpoint=True)
                    # Get random colors
                    nb_colors = self.rng.integers(3, 4 if max_series_len >= 4 else 3, endpoint=True)
                    rand_colors = self.rng.choice(self.ref_names.COLORS, nb_colors, replace=False)
                    b_valid = self._check_color_series(tile_counter=tile_counter, colors=rand_colors, number=rand_nb)

                # Generate series
                for i, color in enumerate(rand_colors):
                    # rotate_angle = random.randint(-5, 5)  # Not needed for now
                    tile_name = self.ref_names.get_tile(color=color, number=rand_nb)
                    # Randomly replace tile with joker in some cases
                    tile_name = self._random_joker(tile_counter=tile_counter, tile_name=tile_name, prob=0.05)
                    tile_counter[tile_name] += 1

                    # Add tile to image
                    curr_pos, img, bbox = self._add_tile(img, tile_name, input_pos=curr_pos,
                                                         b_update_y=(i == len(rand_colors)-1))
                    # Update bboxes
                    tile_bboxes.append((tile_name, bbox))
                    self._update_series_bbox(series_bbox, bbox)

                new_pos_x = self.rng.integers(25, 75, endpoint=True)
                new_pos_y = curr_pos[1] + self.rng.integers(50, 100, endpoint=True)
                curr_pos = (new_pos_x, new_pos_y)
                bboxes[f'color_{rand_nb}_' + '_'.join(rand_colors)] = tile_bboxes
                series_bbox = tuple((x,y) for x,y in zip(['UL', 'UR', 'BL', 'BR'], series_bbox))
                series_bboxes[f'color_{rand_nb}_' + '_'.join(rand_colors)] = series_bbox

        # ############################################################
        # DEBUG: Check bboxes
        # ############################################################
        # from PIL import ImageDraw
        # draw = ImageDraw.Draw(img)
        # Check tile bboxes
        # for tpl in bboxes:
        #     for e in tpl[1]:
        #         bbox = e[1]
        #         xy = (bbox[0][0], bbox[0][1], bbox[3][0], bbox[3][1])
        #         print(xy)
        #         draw.rectangle(xy, width=1)
        # Check series bboxes
        # for tpl in series_bboxes:
        #     print(tpl)
        #     xy = (tpl[1][0][0], tpl[1][0][1], tpl[1][3][0], tpl[1][3][1])
        #     print(xy)
        #     draw.rectangle(xy, width=1)

        if b_show:
            img.show()

        return img, bboxes, series_bboxes

    def generate_invalid_img(self, width=None, height=None,
                             max_nb_series=4,
                             max_series_len=13,
                             b_use_jokers=True,
                             b_disco_mode=True,
                             bg_color=(255, 255, 255, 255),
                             b_show=False):
        """
        This method will generate a random image containing invalid Rummikub tile groups. First, a valid series
        will be generated, followed by random replacement of one tile in the valid series with an invalid one.

        :param width: image width in pixels, if 'None' will be set automatically
        :param height: image heigth in pixels, if 'None' will be set automatically
        :param max_nb_series: maximum number of series generate
        :param max_series_len: maximum length of any one series (max. is 13, so no series longer than this will be
         generated); should be 3 <= max_series_len <=13
        :param b_use_jokers: allow the usage of jokers or not, 'True' by default
        :param b_disco_mode: randomly generate a background color!
        :param bg_color: background color to use if b_disco_mode is False
        :param b_show: show generated image
        :return:
        """
        if max_series_len < 3 or max_series_len > 13:
            raise ValueError(f"Invalid max_series_len, should be 3 <= x <= 13, got {max_series_len}.")

        # Keep track of used tiles
        tile_counter = Counter()
        if not b_use_jokers:
            for e in self.ref_names.jokers:
                tile_counter[e] = 1

        # Set width such that it can surely contain the maximum length series
        if width is None:
            width = int(100 + (max_series_len * self.ril.img_width * self.resize))
            if self.b_add_pos_noise:
                width += (max_series_len-1) * self.max_pos_offset
        self.width = width
        # Set height such that it can surely contain the maximum number of series
        if height is None:
            height = int(150 + (max_nb_series * self.ril.img_heigth * self.resize))
            if self.b_add_pos_noise:
                height += (max_nb_series-1) * self.max_pos_offset
        self.height = height

        bboxes = OrderedDict()
        series_bboxes = OrderedDict()

        if b_disco_mode:
            bg_color = tuple(self.rng.integers(0, 255, 3, endpoint=True)) + (255,)
        img = Image.new(mode='RGBA', size=(width, height), color=bg_color)

        starting_point = (self.rng.integers(25, 75, endpoint=True), self.rng.integers(50, 100, endpoint=True))
        curr_pos = starting_point

        nb_series = self.rng.integers(1, max_nb_series, endpoint=True)
        for _ in range(nb_series):
            # First, decide what we are gonna do: a '1-2-3' series, or a 'color' series?
            # Let's say: <0.5 = '1-2-3', >=0.5 = 'color'.

            # bbox of the entire series: UL, UR, BL, BR
            series_bbox = [[math.inf, math.inf], [-math.inf, math.inf],
                           [math.inf, -math.inf], [-math.inf, -math.inf]]
            tile_bboxes = []

            b_disrupted = False  # Check if series has already been made invalid
            # Generate '1-2-3' series.
            if self.rng.random() < 0.5:
                # rotate_angle = random.randint(-5, 5)  # Not needed for now
                # Get valid random series
                b_valid = False
                nb_iters = 0  # Let's build in an infinite-loop breaker, just in case...
                while b_valid is False:
                    nb_iters += 1
                    if nb_iters == 100:
                        raise StopIteration("This thing has been going on for a while now. It would appear something "
                                            "might be off?")
                    # Get starting tile
                    series_len = self.rng.integers(3, max_series_len, endpoint=True)
                    tile_name, tile_color, tile_number = self.ref_names.get_random_tile(max_number=14-series_len)
                    b_valid = self._check_num_series(tile_counter, tile_color, tile_number, tile_number+series_len)

                # Generate series
                odds_per_tile = 1/series_len  # Odds of replacing tile at pos X with invalid tile
                for i in range(0, series_len):
                    tile_name = self.ref_names.get_tile(color=tile_color, number=(tile_number+i) % 13)
                    # Randomly replace tile with joker in some cases
                    tile_name = self._random_joker(tile_counter=tile_counter, tile_name=tile_name, prob=0.05)
                    tile_counter[tile_name] += 1

                    # Randomly replace valid tile with invalid tile?
                    if not b_disrupted and ((i == series_len-1) or self.rng.random() < odds_per_tile):
                        while True:
                            _tile_name, _tile_color, _tile_number = self.ref_names.get_random_tile()
                            # Check with tile_color and tile_number in case tile has been replaced with joker
                            if (_tile_color != tile_color or _tile_number != tile_number) and\
                                    tile_counter[_tile_name] < 2:
                                # Re-make joker available, in case original tile was replace by joker
                                if tile_name.startswith('J_'):
                                    tile_counter[tile_name] -= 1
                                # Replace valid tile with invalid one
                                tile_name = _tile_name
                                break
                        b_disrupted = True

                    # Add tile to image
                    curr_pos, img, bbox = self._add_tile(img, tile_name, input_pos=curr_pos,
                                                         b_update_y=(i == series_len-1))
                    # Update bboxes
                    tile_bboxes.append((tile_name, bbox))
                    self._update_series_bbox(series_bbox, bbox)

                new_pos_x = self.rng.integers(25, 75, endpoint=True)
                new_pos_y = curr_pos[1] + self.rng.integers(50, 100, endpoint=True)
                curr_pos = (new_pos_x, new_pos_y)
                bboxes[f'123_{tile_color}_{tile_number}_{tile_number+series_len-1}'] = tile_bboxes
                series_bbox = tuple(tuple(x) for x in series_bbox)
                series_bboxes[f'123_{tile_color}_{tile_number}_{tile_number+series_len-1}'] = series_bbox
            # Generate 'color' series
            else:
                b_valid = False
                nb_iters = 0  # Let's build in an infinite-loop breaker, just in case...
                while b_valid is False:
                    nb_iters += 1
                    if nb_iters == 100:
                        raise StopIteration("This thing has been going on for a while now. It would appear something "
                                            "might be off?")
                    # Get random number
                    rand_nb = self.rng.integers(1, 13, endpoint=True)
                    # Get random colors
                    nb_colors = self.rng.integers(3, 4 if max_series_len >= 4 else 3, endpoint=True)
                    rand_colors = self.rng.choice(self.ref_names.COLORS, nb_colors, replace=False)
                    b_valid = self._check_color_series(tile_counter=tile_counter, colors=rand_colors, number=rand_nb)

                # Generate series
                odds_per_tile = 1/nb_colors  # Odds of replacing tile at pos X with invalid tile
                for i, color in enumerate(rand_colors):
                    # rotate_angle = random.randint(-5, 5)  # Not needed for now
                    tile_name = self.ref_names.get_tile(color=color, number=rand_nb)
                    # Randomly replace tile with joker in some cases
                    tile_name = self._random_joker(tile_counter=tile_counter, tile_name=tile_name, prob=0.05)
                    tile_counter[tile_name] += 1

                    # Randomly replace valid tile with invalid tile?
                    if not b_disrupted and ((i == nb_colors-1) or self.rng.random() < odds_per_tile):
                        while True:
                            _tile_name, _tile_color, _tile_number = self.ref_names.get_random_tile()
                            # Check with tile_color and tile_number in case tile has been replaced with joker
                            # ! ATTENTION ! In the case of a color series, in the case of a 3 color series there are
                            # small odds that the randomly picked 'invalid' tile is actually valid, in case the
                            # chosen tile has the same number and is of the fourth color.
                            if _tile_number == rand_nb and _tile_color not in rand_colors:
                                continue
                            if (_tile_color != color or _tile_number != rand_nb) and\
                                    tile_counter[_tile_name] < 2:
                                # Re-make joker available, in case original tile was replace by joker
                                if tile_name.startswith('J_'):
                                    tile_counter[tile_name] -= 1
                                # Replace valid tile with invalid one
                                tile_name = _tile_name
                                break
                        b_disrupted = True

                    # Add tile to image
                    curr_pos, img, bbox = self._add_tile(img, tile_name, input_pos=curr_pos,
                                                         b_update_y=(i == len(rand_colors)-1))
                    # Update bboxes
                    tile_bboxes.append((tile_name, bbox))
                    self._update_series_bbox(series_bbox, bbox)

                new_pos_x = self.rng.integers(25, 75, endpoint=True)
                new_pos_y = curr_pos[1] + self.rng.integers(50, 100, endpoint=True)
                curr_pos = (new_pos_x, new_pos_y)
                bboxes[f'color_{rand_nb}_' + '_'.join(rand_colors)] = tile_bboxes
                series_bbox = tuple((x,y) for x,y in zip(['UL', 'UR', 'BL', 'BR'], series_bbox))
                series_bboxes[f'color_{rand_nb}_' + '_'.join(rand_colors)] = series_bbox

        # ############################################################
        # DEBUG: Check bboxes
        # ############################################################
        # from PIL import ImageDraw
        # draw = ImageDraw.Draw(img)
        # Check tile bboxes
        # for tpl in bboxes:
        #     for e in tpl[1]:
        #         bbox = e[1]
        #         xy = (bbox[0][0], bbox[0][1], bbox[3][0], bbox[3][1])
        #         print(xy)
        #         draw.rectangle(xy, width=1)
        # Check series bboxes
        # for tpl in series_bboxes:
        #     print(tpl)
        #     xy = (tpl[1][0][0], tpl[1][0][1], tpl[1][3][0], tpl[1][3][1])
        #     print(xy)
        #     draw.rectangle(xy, width=1)

        if b_show:
            img.show()

        return img, bboxes, series_bboxes

    def generate_bbox_images(self, nb_images, out_dir, b_create_dir=False, b_clear_dir=False,
                             b_disco_mode=True, bg_color=(255, 255, 255, 255), **kwargs):
        """
        Generate (valid) images and save corresponding bounding box information to HD.

        :param nb_images: number of images to generate
        :param out_dir: output directory images and bounding boxes will be saved to
        :param b_create_dir: create output directory if it doesn't exist yet
        :param b_clear_dir: clear directory if it already exists and contains items
        :param b_disco_mode: randomly generate a background color!
        :param bg_color: background color to use if b_disco_mode = False
        :param kwargs: parameters to be passed on to generate_img
        :return:
        """
        if not os.path.isdir(out_dir):
            if b_create_dir:
                os.makedirs(out_dir)
            else:
                raise ValueError(f"Output directory does not yet exist, and b_create_dir=False: [{out_dir}]")
        elif b_clear_dir:
            shutil.rmtree(out_dir)
            os.makedirs(out_dir)
        else:
            print("WARNING! Specified output directory already exists! Existing items will be overwritten.")
            print("If you happen to be generating less images than already exist in the output directory,")
            print("the old files will still remain.")

        for i in range(nb_images):
            print(f"\rGenerating image {i+1}/{nb_images}...", end='', flush=True)
            img_name = f'img_{i+1:03d}'
            img, bboxes, series_boxes = self.generate_valid_img(b_disco_mode=b_disco_mode, bg_color=bg_color, **kwargs)
            img.save(os.path.join(out_dir, f'{img_name}.png'), format='PNG')
            with open(os.path.join(out_dir, f'{img_name}_bboxes.json'), 'w') as fout:
                fout.write(json.dumps(bboxes, indent=2, cls=NPEncoder))
            with open(os.path.join(out_dir, f'{img_name}_series_bboxes.json'), 'w') as fout:
                fout.write(json.dumps(series_boxes, indent=2, cls=NPEncoder))
        print('\n')

    def generate_deepproblog_sum_images(self, nb_images, out_dir, b_create_dir=False, b_clear_dir=False,
                                        b_disco_mode=True, bg_color=(255, 255, 255, 255), **kwargs):
        """
        Generate images and save labels for DeepProbLog training to HD.
        The problem to be solved is: compute the sum of a series of 3 tiles.

        :param nb_images: number of images to generate
        :param out_dir: output directory images and bounding boxes will be saved to
        :param b_create_dir: create output directory if it doesn't exist yet
        :param b_clear_dir: clear directory if it already exists and contains items
        :param b_disco_mode: randomly generate a background color!
        :param bg_color: background color to use if b_disco_mode = False
        :param kwargs: parameters to be passed on to generate_img
        :return:
        """
        if not os.path.isdir(out_dir):
            if b_create_dir:
                os.makedirs(out_dir)
            else:
                raise ValueError(f"Output directory does not yet exist, and b_create_dir=False: [{out_dir}]")
        elif b_clear_dir:
            shutil.rmtree(out_dir)
            os.makedirs(out_dir)
        else:
            print("WARNING! Specified output directory already exists! Existing items will be overwritten.")
            print("If you happen to be generating less images than already exist in the output directory,")
            print("the old files will still remain.")

        for i in range(nb_images):
            if (i+1) % 25 == 0:
                print(f"\rGenerating image {i+1}/{nb_images}...", end='', flush=True)
            img_name = f'img_{i+1:03d}'
            img, bboxes, series_boxes = self.generate_valid_img(b_disco_mode=b_disco_mode, bg_color=bg_color, **kwargs)
            img.save(os.path.join(out_dir, f'{img_name}.png'), format='PNG')
            # Write type of series and sum of tiles
            with open(os.path.join(out_dir, f'{img_name}.txt'), 'w') as fout:
                for k in series_boxes.keys():
                    parts = k.split('_')
                    if k.startswith('123'):
                        # Sum of tiles = sum of range starting with lowest tile value (=parts[2])
                        # up until highest tile value (=parts[3])
                        fout.write(f"{k}: {sum(range(int(parts[2]), int(parts[3])+1))}")
                    else:
                        # Sum of tiles = value of tile (=int(parts[1])) * number of tiles in series (=len(parts)-2)
                        fout.write(f"{k}: {int(parts[1])*(len(parts)-2)}")
        print('\n')

    def generate_deepproblog_valid_images(self, nb_images, out_dir, b_create_dir=False, b_clear_dir=False,
                                          b_disco_mode=True, bg_color=(255, 255, 255, 255), **kwargs):
        """
        Generate images for DeepProbLog training to HD.
        The problem to be solved is: check whether a series of tiles is valid or not.
        The label of the image (0 for false, 1 for true) is stored in the filename, not in a separate file.

        :param nb_images: number of images to generate
        :param out_dir: output directory images and bounding boxes will be saved to
        :param b_create_dir: create output directory if it doesn't exist yet
        :param b_clear_dir: clear directory if it already exists and contains items
        :param b_disco_mode: randomly generate a background color!
        :param bg_color: background color to use if b_disco_mode = False
        :param kwargs: parameters to be passed on to generate_img
        :return:
        """
        if not os.path.isdir(out_dir):
            if b_create_dir:
                os.makedirs(out_dir)
            else:
                raise ValueError(f"Output directory does not yet exist, and b_create_dir=False: [{out_dir}]")
        elif b_clear_dir:
            shutil.rmtree(out_dir)
            os.makedirs(out_dir)
        else:
            print("WARNING! Specified output directory already exists! Existing items will be overwritten.")
            print("If you happen to be generating less images than already exist in the output directory,")
            print("the old files will still remain.")

        if nb_images % 2 != 0:
            raise ValueError("Number of images to generate should be divisible by 2, as half the generated images\n"
                             f"will be valid, and the other half invalid.\nGot {nb_images} instead.")

        for i in range(nb_images):
            if (i+1) % 25 == 0:
                print(f"\rGenerating image {i+1}/{nb_images}...", end='', flush=True)
            # Generate invalid image
            if i % 2 == 0:
                img, bboxes, series_boxes = self.generate_invalid_img(b_disco_mode=b_disco_mode, bg_color=bg_color, **kwargs)
            # Generate valid image
            else:
                img, bboxes, series_boxes = self.generate_valid_img(b_disco_mode=b_disco_mode, bg_color=bg_color, **kwargs)

            img_name = f'{i%2}_img_{i+1:03d}'
            img.save(os.path.join(out_dir, f'{img_name}.png'), format='PNG')
            # # Write type of series and sum of tiles
            # with open(os.path.join(out_dir, f'{img_name}.txt'), 'w') as fout:
            #     for k in series_boxes.keys():
            #         parts = k.split('_')
            #         if k.startswith('123'):
            #             fout.write(f"{k}: {sum(range(int(parts[2]), int(parts[3])+1))}")
            #         else:
            #             fout.write(f"{k}: {int(parts[1])*(len(parts)-2)}")
        print('\n')

    # ############################################################
    # Here be Dragons!
    # ############################################################
    def _add_tile(self, host_img: Image, guest_img: str, input_pos: Tuple[int, int], rotate_angle: int = 0, b_update_y=False):
        """

        :param host_img:
        :param guest_img:
        :param input_pos: position in the host_img at which to place the guest_img
        :param rotate_angle:
        :param b_update_y:
        :return:
        """
        orig_input_pos = input_pos
        # tile = Image.open(self.ril.get_ref_img(guest_img))
        tile = self.ril.get_ref_img(guest_img)
        if self.resize != 1:
            tile = tile.resize((int(tile.size[0]*self.resize), int(tile.size[1]*self.resize)))
        orig_size = tile.size
        # "Global" rotation applied to entire series
        if rotate_angle:
            raise NotImplementedError
            # tile = tile.rotate(rotate_angle, expand=True, fillcolor=(0, 0, 0, 0))
            # print("!!! UPDATE BBOX TO ALLOW FOR ROTATED IMAGES !!!")

        # Add positional noise
        offset_x, offset_y = 0, 0
        if self.b_add_pos_noise:
            offset_x, offset_y = self.rng.integers(0, self.max_pos_offset, 2)
            input_pos = (input_pos[0]+offset_x, input_pos[1]+offset_y)

        # Add local rotational noise
        loc_rot_angle = 0.
        if self.b_add_rot_noise:
            # Draw random rotation angle from normal distribution with center = 0 and mean = max_rot_angle.
            # Verify drawn sample is valid (i.e., does not exceed the max value).
            loc_rot_angle = self.rng.normal(loc=0., scale=self.max_rot_angle)
            while math.fabs(loc_rot_angle) > self.max_rot_angle:
                loc_rot_angle = self.rng.normal(loc=0., scale=self.max_rot_angle)
            tile = tile.rotate(loc_rot_angle, expand=True, fillcolor=(0, 0, 0, 0))

        # !!! ATTENTION !!!
        # The most obvious thing to do might seem to be:
        #   host_img.paste(tile, box=box)
        # However, for some obscure reason this method does not correctly take into account the alpha layer.
        # To generate correctly overlapping alpha channels (i.e., transparency), the images must be added to the
        # main image in the following slightly more convoluted way.
        temp_img = Image.new(mode='RGBA', size=(self.width, self.height), color=(0, 0, 0, 0))
        temp_img.paste(tile, box=input_pos)

        host_img = Image.alpha_composite(host_img, temp_img)
        curr_pos = self._update_pos(orig_input_pos, (offset_x, offset_y),
                                    tile.size, rotate_angle=rotate_angle, b_update_y=b_update_y)
        tile.close()

        # Bounding Box: UL, UR, BL, BR
        # TODO: Update in case of global rotation; local rotation is no problem
        bbox = (('UL', input_pos),  # UL
                ('UR', (input_pos[0] + tile.size[0], input_pos[1])),  # UR
                ('BL', (input_pos[0], input_pos[1] + tile.size[1])),  # BL
                ('BR', (input_pos[0] + tile.size[0], input_pos[1] + tile.size[1])))  # BR

        return curr_pos, host_img, bbox

    def _update_pos(self, curr_pos, rand_offset: Tuple[int, int], tile_size: Tuple[int, int],
                    rotate_angle: int, b_update_y=False):
        """
        Update position so that it becomes UL position of the next tile to be placed.

        :param curr_pos:
        :param rand_offset: random offset applied to curr_pos
        :param tile_size: tile size, after application of rotation noise
        :param rotate_angle: "global" rotation angle
        :param b_update_y: whether to update y-position or not
        :return:
        """
        new_x = curr_pos[0] + rand_offset[0] + math.floor(math.cos(math.radians(rotate_angle))*tile_size[0])
        new_y = curr_pos[1] - math.floor(math.sin(math.radians(rotate_angle))*tile_size[0])
        if b_update_y:
            new_y += tile_size[1]

        update = (new_x, new_y)

        return update

    def _update_series_bbox(self, series_bbox, bbox):
        """

        :param series_bbox: list[4x[x,y]], i.e., list containing 4 2-item lists containing the series bbox positions,
        in order UL, UR, BL, BR
        :param bbox: tpl(x, y), bbox of the item to use to update the series bbox with
        :return:
        """
        bbox_x_lo, bbox_x_hi = bbox[0][1][0], bbox[1][1][0]
        bbox_y_lo, bbox_y_hi = bbox[0][1][1], bbox[2][1][1]
        if bbox_x_lo < series_bbox[0][0]:
            series_bbox[0][0] = bbox_x_lo
            series_bbox[2][0] = bbox_x_lo
        if bbox_x_hi > series_bbox[1][0]:
            series_bbox[1][0] = bbox_x_hi
            series_bbox[3][0] = bbox_x_hi
        if bbox_y_lo < series_bbox[0][1]:
            series_bbox[0][1] = bbox_y_lo
            series_bbox[1][1] = bbox_y_lo
        if bbox_y_hi > series_bbox[2][1]:
            series_bbox[2][1] = bbox_y_hi
            series_bbox[3][1] = bbox_y_hi

    def _random_joker(self, tile_counter: Counter, tile_name, prob=0.05):
        """
        Replace tile with joker tile with probability = (prob*100)%.

        :param tile_counter: Counter object containing the used tiles and the number of times they have been used.
        :param tile_name: name of the tile to (possibly) be replaced with a joker
        :return: joker tile with probability (prob*100)%, else the provided tile_name
        """
        b_used_jk = (tile_counter[self.ref_names.JKR_BLACK] > 0)
        b_used_jr = (tile_counter[self.ref_names.JKR_RED] > 0)
        if (not b_used_jr or not b_used_jk) and self.rng.random() < prob:
            # Black joker has not been used
            if not b_used_jk:
                # Red joker has also not been used
                if not b_used_jr:
                    # Randomly pick a joker
                    rand_jkr = self.rng.integers(0, 1, endpoint=True)
                    tile_name = self.ref_names.jokers[rand_jkr]
                # Red joker has been used
                else:
                    tile_name = self.ref_names.JKR_BLACK
            # Black joker has been used
            else:
                tile_name = self.ref_names.JKR_RED

        return tile_name

    def _check_num_series(self, tile_counter, color, start, end, b_debug=False):
        """
        Check if it is possible to create a certain numerical series with the remaining tiles.

        :param tile_counter: Counter object containing the used tiles and the number of times they have been used.
        :param color: color of the series
        :param start: starting number of the series (inclusive)
        :param end: ending number of the series (exclusive)
        :return: True if it is still possible to create this series given the current status of tile_counter,
        False otherwise
        """
        color_chr = self.ref_names.colors_to_chr[color]
        for i in range(start, end):
            # Each non-joker tile can only appear at most twice
            if tile_counter[f'{i}_{color_chr}'] == 2:
                if b_debug:
                    print("This baby ain't gonna fly no more...")
                return False

        return True

    def _check_color_series(self, tile_counter, colors, number, b_debug=False):
        """
        Check if it is possible to create a certain color series with the remaining tiles.

        :param tile_counter: Counter object containing the used tiles and the number of times they have been used.
        :param color: color of the series
        :param start: starting number of the series (inclusive)
        :param end: ending number of the series (exclusive)
        :return: True if it is still possible to create this series given the current status of tile_counter,
        False otherwise
        """

        for c in colors:
            # Each non-joker tile can only appear at most twice
            if tile_counter[f'{number}_{self.ref_names.colors_to_chr[c]}'] == 2:
                if b_debug:
                    print("This baby ain't gonna fly no more...")
                return False

        return True


if __name__ == '__main__':
    img_gen = ImageGenerator(b_add_pos_noise=True,
                             max_pos_offset=10,
                             b_add_rot_noise=False,
                             max_rot_angle=3)
    # img_gen.generate_invalid_img(max_nb_series=1, max_series_len=6, b_show=True)
    # print(img_gen.generate()[1])
    img_gen.generate_deepproblog_sum_images(nb_images=5000,
                                            out_dir=os.path.join(RRIGConfig.HOME, 'Work', 'Projects', 'DeepProbLog-ImgReasoning',
                                                             'Data', 'Rummikub_5000'),
                                            b_create_dir=True, b_clear_dir=True,
                                            width=464,
                                            height=320,
                                            max_nb_series=1,
                                            max_series_len=3,
                                            b_use_jokers=False)
    # img_gen.generate_deepproblog_valid_images(nb_images=2000,
    #                                           out_dir=os.path.join(RRIGConfig.HOME, 'Work', 'Projects',
    #                                                                'DeepProbLog-ImgReasoning', 'Data',
    #                                                                'Rummikub_Valid'),
    #                                           b_create_dir=True, b_clear_dir=True,
    #                                           width=464,
    #                                           height=320,
    #                                           max_nb_series=1,
    #                                           max_series_len=3,
    #                                           b_use_jokers=False)
