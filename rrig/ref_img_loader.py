"""
This script will load the reference images representing the individual Rummikub tiles.

'k' = 'key' = 'black', so as to be able to use 'b' for 'blue'

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
import random

from config import RRIGConfig

from PIL import Image


class RefImgs:
    BLACK = 'black'
    BLUE = 'blue'
    ORANGE = 'orange'
    RED = 'red'
    COLORS = [BLACK, BLUE, ORANGE, RED]
    JKR_BLACK = 'J_K'
    JKR_RED = 'J_R'

    def __init__(self):
        self.series_blue = [f'{i}_B' for i in range(1, 14)]
        self.series_black = [f'{i}_K' for i in range(1, 14)]
        self.series_orange = [f'{i}_O' for i in range(1, 14)]
        self.series_red = [f'{i}_R' for i in range(1, 14)]
        self.jokers = [self.JKR_BLACK, self.JKR_RED]

        self.colors_to_chr = {self.BLUE: 'B', self.BLACK: 'K', self.ORANGE: 'O', self.RED: 'R'}
        self.colors_to_idx = {self.BLUE: 0, self.BLACK: 1, self.ORANGE: 2, self.RED: 3}
        self.idx_to_colors = {0: self.BLUE, 1: self.BLACK, 2: self.ORANGE, 3: self.RED}
        self.idx_to_series = {0: self.series_blue, 1: self.series_black, 2: self.series_orange, 3: self.series_red}
        self.nb_series = 4

    def get_random_tile(self, max_number=13) -> (str, int):
        """
        Returns a random tile from a random series. Will NOT return a joker, only a digit.

        :param max_number: the highest tile number (i.e., value on the tile) that can be returned
        :return: name of tile, color of tile, number on tile
        """
        rand_series = random.randint(0, self.nb_series - 1)
        rand_idx = random.randint(0, max_number - 1)

        return self.idx_to_series[rand_series][rand_idx], self.idx_to_colors[rand_series], rand_idx + 1

    def get_tile(self, color: str, number):
        """
        Get tile from series of color 'color' representing 'number'. No jokers can be retrieved this way.

        :param color: the color of the series to pick from
        :param number: the number represented on the tile
        :return:
        """
        # 'number-1' because that's the index corresponding to the tile with 'number' on it...
        return self.idx_to_series[self.colors_to_idx[color]][number - 1]


class RefImgLoader:
    def __init__(self, dir_ref: str = None, b_check_equal_size=True):
        """

        :param dir_ref: directory containing the images to load.
        :param b_check_equal_size: if set to True, will throw an error if not all loaded images have the same size.
        """
        if dir_ref is None:
            dir_ref = RRIGConfig.DIR_REF_IMGS

        img_width, img_height = 0, 0

        ref_imgs = {}
        for i, f in enumerate(os.listdir(dir_ref)):
            full_path = os.path.join(dir_ref, f)
            if not f.endswith('png') or not os.path.isfile(full_path):
                continue

            # Get image size and copy image to memory
            with Image.open(full_path) as img:
                ref_imgs[f.split('.')[0]] = img.copy()
                if i == 0:
                    img_width, img_height = img.size
                elif b_check_equal_size and img_width != img.size[0] or img_height != img.size[1]:
                    raise ValueError("Not all images have the same size.")

        self.img_width, self.img_heigth = img_width, img_height
        self.ref_imgs = ref_imgs

    def get_ref_img(self, ref_img: str):
        return self.ref_imgs[ref_img]


if __name__ == '__main__':
    ril = RefImgLoader()
