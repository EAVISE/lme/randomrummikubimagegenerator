# Random Rummikub Image Generator

This tool, the Random Rummikub Image Generator (RRIG) will change your life, for the better, forever. It is one of those
things that you didn't know existed, yet when they *finally* do enter your life, you can only wonder how you managed to
live so long without having known the joys it procures you.

Don't worry, we understand. Perfectly. But rest assured, from now on, things will only be getting better and better.

Yes, it is true. This application will actually really truly *for realz* randomly generate valid Rummikub series.

"How then, pray tell me Brother, can we make use of this magnificent invention?"

"Tegoei en ni verkeerd", as the people of Flandria would say. In Pythonese, this translates to:

    from rrig.img_generator import ImageGenerator

    img_gen = ImageGenerator()
    img_gen.generate_valid_img(b_show=True)

This will generate a single image containing a valid Rummikub tile series, and show the result on screen. Very importantly,
the `generate_valid_img` method has a parameter `b_disco_mode` which, when set to `True`, will let the method generate a
random background color to be used for the image! *Groovy!* The `ImageGenerator` supports adding random positional variation
and random rotational variation for each tile. By default, this is disabled. To enable, initialize the `ImageGenerator`
instance the following way, and feel free to update `max_pos_offset` and `max_rot_angle` to suit your own needs:

    img_gen = ImageGenerator(b_add_pos_noise=True,
                             max_pos_offset=20,
                             b_add_rot_noise=False,
                             max_rot_angle=2)

You can also generate an *invalid* Rummikub tile series using:

    img_gen.generate_invalid_img(b_show=True)

This method will first generate a valid series, and then randomly replace one tile in the series with an invalid one.

In case you are feeling adventurous and wish to generate multiple images at once, to be stored, together with the
bounding boxes of the objects displayed in the images, in some folder on your HD, use:

    # set b_create_dir=True to create out_dir if it doesn't already exist
    img_gen.generate_bbox_images(nb_images=100, out_dir='generated/', b_create_dir=False)

This will generate 100 valid images (of course, you can change this number to reflect your own preference), which will be
stored as "img_xxx.png" for the image, "img_xxx_bboxes.json" for the corresponding bounding boxes of the individual
tiles grouped per series, and "img_xxx_series_bboxes.json" for the bounding boxes of each series as a whole, where
of course "xxx" does not refer to the mind-blowing Vin Diesel action movie feat. Rammstein, but to the id of the
generated image.

For your convenience, two other methods are available that will generate images that can be used for two specific
research problems using DeepProbLog.

* `generate_deepproblog_sum_images()`: This method will generate images that contain (valid) series of 3 tiles. Besides 
saving the images, also the sum of the tiles are saved to disk. If the images are saved to "img_xxx.png", then the sums
are saved to "img_xxx.txt", i.e., one file per image. The name of the game then becomes to write a DeepProbLog
program that correctly computes the sum of the tiles. Note that bounding boxes are **not** saved, as they are not 
needed for solving this problem using DeepProbLog.
* `generate_deepproblog_valid_images()`: This method will alternately generate a valid and an invalid series. 
Valid images are stored as "1_img_xxx.png", invalid images as "0_img_xxx.png". The name of the game then becomes to write a
DeepProbLog program that correctly predicts whether a series is valid or not. If you want to go one step further,
you could attempt to write a program that not only correctly detects invalid series, but also tells you what to do
in order to make the series valid. Note that bounding boxes are **not** saved, as they are not needed for solving
this problem using DeepProbLog.

ATTENTION! Make sure to update `config.py` to reflect your local configuration.

Now, go forth and spread the word.

Author: Laurent Mertens  
Mail: laurent.mertens@kuleuven.be
